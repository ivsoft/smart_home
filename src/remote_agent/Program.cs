﻿using Iot.Device.DHTxx;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

namespace remote_agent
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = new HostBuilder()
                   .ConfigureAppConfiguration((hostingContext, config) =>
                   {
                       config.SetBasePath(Directory.GetCurrentDirectory());
                       config.AddJsonFile("appsettings.json", true);
                       if (args != null) config.AddCommandLine(args);
                   })
                   .ConfigureServices((hostingContext, services) =>
                   {
                       var app = new Startup(hostingContext.Configuration);
                       app.ConfigureServices(services);
                   })
                   .ConfigureLogging((hostingContext, logging) =>
                   {
                       logging.AddConfiguration(hostingContext.Configuration);
                       logging.AddConsole();
                   });

            await builder.RunConsoleAsync();
        }
    }
}

﻿using IVySoft.VPlatform.Target.Notifications;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Quartz;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace remote_agent
{
    internal class Startup
    {
        private IConfiguration Configuration;

        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.UseNotifications();
            services.AddSingleton<ISensorStorage, SensorStorage>();


            services.Configure<DHTSensorConfig>(this.Configuration.GetSection("DHT"));
            services.Configure<EmailSettings>(this.Configuration.GetSection("EmailSettings"));
            services.Configure<SendMailConfig>(this.Configuration.GetSection("EmailConfig"));

            // base configuration from appsettings.json
            services.Configure<QuartzOptions>(Configuration.GetSection("Quartz"));

            // if you are using persistent job store, you might want to alter some options
            services.Configure<QuartzOptions>(options =>
            {
                options.Scheduling.IgnoreDuplicates = true; // default: false
                options.Scheduling.OverWriteExistingData = true; // default: true
            });

            services.AddQuartz(q =>
            {
                q.UseMicrosoftDependencyInjectionJobFactory();
            });

            services.Configure<JobConfig>(Configuration.GetSection("Jobs"));
            services.AddOptions<QuartzOptions>()
                .Configure<IOptions<JobConfig>>((options, dep) =>
                {
                    if (string.IsNullOrWhiteSpace(dep.Value.ReadDHTSensor))
                    {
                        throw new Exception($"No Quartz.NET Cron schedule found for job in configuration at ReadDHTSensor");
                    }
                    var jobKey = new JobKey("ReadDHTSensor", "custom");
                        options.AddJob<ReadDHTSensorJob>(j => j.WithIdentity(jobKey));
                        options.AddTrigger(trigger => trigger
                            .WithIdentity("ReadDHTSensor-trigger", "custom")
                            .ForJob(jobKey)
                            .WithCronSchedule(dep.Value.ReadDHTSensor));

                    if (string.IsNullOrWhiteSpace(dep.Value.SendEmail))
                    {
                        throw new Exception($"No Quartz.NET Cron schedule found for job in configuration at SendEmail");
                    }
                    jobKey = new JobKey("SendMail", "custom");
                    options.AddJob<SendEmailJob>(j => j.WithIdentity(jobKey));
                    options.AddTrigger(trigger => trigger
                        .WithIdentity("SendMail-trigger", "custom")
                        .ForJob(jobKey)
                        .WithCronSchedule(dep.Value.SendEmail));
                });

            // Quartz.Extensions.Hosting allows you to fire background service that handles scheduler lifecycle
            services.AddQuartzHostedService(options =>
            {
                    // when shutting down we want jobs to complete gracefully
                    options.WaitForJobsToComplete = true;
            });
        }

        public void Configure(IServiceProvider sp, IHostEnvironment env)
        {
        }
    }
}
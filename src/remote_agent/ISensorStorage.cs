﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace remote_agent
{
    public interface ISensorStorage
    {
        IEnumerable<KeyValuePair<DateTime, SensorValue>> Values { get; }

        ValueTask WriteSensor(SensorValue value);
        void Clear();
    }
}
﻿namespace remote_agent
{
    public struct SensorValue
    {
        public double Temperature;
        public double Humidity;
    }
}
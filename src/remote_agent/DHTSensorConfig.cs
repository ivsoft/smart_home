﻿namespace remote_agent
{
    public class DHTSensorConfig
    {
        public int Pin { get; set; } = 4;
        public double ReadPeriod { get; set; } = 1;
        public int ReadCount { get; set; } = 10;
    }
}
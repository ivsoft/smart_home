﻿namespace remote_agent
{
    internal class JobConfig
    {
        public string ReadDHTSensor { get; set; }
        public string SendEmail { get; set; }
    }
}
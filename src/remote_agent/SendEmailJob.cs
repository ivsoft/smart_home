﻿using IVySoft.VPlatform.Target.Notifications;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Quartz;
using System;
using System.Text;
using System.Threading.Tasks;

namespace remote_agent
{
    [DisallowConcurrentExecution]
    internal class SendEmailJob : IJob
    {
        private readonly SendMailConfig _config;
        private readonly ISensorStorage _storage;
        private readonly IEmailSender _sender;
        private readonly ILogger<SendEmailJob> _logger;

        public SendEmailJob(IOptions<SendMailConfig> options, ISensorStorage storage, IEmailSender sender, ILogger<SendEmailJob> logger)
        {
            _config = options.Value;
            _storage = storage;
            _sender = sender;
            _logger = logger;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var sb = new StringBuilder();
            foreach(var item in _storage.Values)
            {
                sb.AppendLine($"{item.Key};{item.Value.Temperature};{item.Value.Humidity}");

            }
            await _sender.SendEmailAsync(_config.email, _config.subject, sb.ToString());
            _logger.LogInformation($"Email has been sent");
            _storage.Clear();
        }
    }
}
﻿using Iot.Device.DHTxx;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace remote_agent
{
    [DisallowConcurrentExecution]
    class ReadDHTSensorJob : IJob
    {
        private readonly DHTSensorConfig _config;
        private readonly ISensorStorage _storage;
        private readonly ILogger<ReadDHTSensorJob> _logger;

        public ReadDHTSensorJob(IOptions<DHTSensorConfig> options, ISensorStorage storage, ILogger<ReadDHTSensorJob> logger)
        {
            _config = options.Value;
            _storage = storage;
            _logger = logger;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var temperature = new Dictionary<double, int>();
            var humidity = new Dictionary<double, int>();
            using (var dht = new Dht11(_config.Pin))
            {
                for (int i = 0; i < _config.ReadCount; ++i)
                {
                    temperature[dht.Temperature.DegreesCelsius] = temperature.GetValueOrDefault(dht.Temperature.DegreesCelsius) + 1;
                    humidity[dht.Humidity.Percent] = humidity.GetValueOrDefault(dht.Humidity.Percent) + 1;

                    await Task.Delay(TimeSpan.FromSeconds(_config.ReadPeriod));
                }
            }
            double? avg_temperature = null;
            foreach (var t in temperature)
            {
                if(t.Value > _config.ReadCount / 2)
                {
                    avg_temperature = t.Key;
                }
            }
            if (avg_temperature.HasValue && avg_temperature.Value > -100)
            {
                foreach (var h in humidity)
                {
                    if (h.Value > _config.ReadCount / 2)
                    {
                        await _storage.WriteSensor(new SensorValue { Temperature = avg_temperature.Value, Humidity = h.Key });
                        return;
                    }
                }
            }
            _logger.LogWarning($"Unable to read sensor. Readed temparatore: {string.Join(';', temperature.Select(x => x.Key + "C " + x.Value + " times"))}, humidity: {string.Join(';', humidity.Select(x => x.Key + "% " + x.Value + " times"))}");
        }
    }
}

﻿namespace remote_agent
{
    public class SendMailConfig
    {
        public string email { get; set; }
        public string subject { get; set; }
    }
}
﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace remote_agent
{
    public class SensorStorage : ISensorStorage
    {
        private readonly List<KeyValuePair<DateTime, SensorValue>> _values = new List<KeyValuePair<DateTime, SensorValue>>();
        private readonly ILogger<SensorStorage> _logger;

        public SensorStorage(ILogger<SensorStorage> logger)
        {
            _logger = logger;
        }

        public IEnumerable<KeyValuePair<DateTime, SensorValue>> Values
        {
            get
            {
                lock (_values)
                {
                    return _values.ToList();
                }
            }
        }

        public void Clear()
        {
            lock (_values)
            {
                _values.Clear();
            }
        }

        public ValueTask WriteSensor(SensorValue value)
        {
            lock (_values)
            {
                _values.Add(new KeyValuePair<DateTime, SensorValue>(DateTime.UtcNow, value));
            }
            _logger.LogInformation($"New sensor value: {value.Temperature}C, {value.Humidity}%");

            return default;
        }
    }
}